# Rustchain
* VERY Basic blockchain node, written in Rust
* All functions are commanded via REST API calls

# Commands
* View the head block:
  * `curl localhost:4200/head`
* View the full chain:
  * `curl localhost:4200/chain`
* Create a transaction:
  * `curl -d '{"sender": "me","recipient": "you","amount": 1}' -H 'Content-Type: application/json' localhost:4200/transaction`
* Mine a block:
  * `curl localhost:4200/mine`
* Commands for interacting with peer nodes:
  * Add a new peer:
    * `curl -d '{"client_url": "http://other.node:4200"}' -H 'Content-Type: application/json' localhost:4200/peer/register`
  * Resolve chain differences with peers:
    * `curl localhost:4200/peer/resolve`