use super::transaction::Transaction;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Block {
    index: u32,
    timestamp: u64,
    transactions: Vec<Transaction>,
    proof: u32,
    previous_hash: Vec<u8>,
}

impl Block {
    pub fn create(index: u32, timestamp: u64, transactions: Vec<Transaction>, proof: u32, previous_hash: Vec<u8>) -> Block {
        Block {
            index: index,
            timestamp: timestamp,
            transactions: transactions,
            proof: proof,
            previous_hash: previous_hash,
        }
    }

    pub fn get_proof(&self) -> u32 {
        self.proof
    }
}