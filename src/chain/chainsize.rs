#[derive(Serialize, Deserialize)]
pub struct ChainSize {
    size: usize
}

impl ChainSize {
    pub fn new(size: usize) -> ChainSize {
        ChainSize {
            size: size,
        }
    }

    pub fn get_size(&self) -> usize {
        self.size
    }
}
