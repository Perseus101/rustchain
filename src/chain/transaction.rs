#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Transaction {
    sender: String,
    recipient: String,
    amount: u32,
}