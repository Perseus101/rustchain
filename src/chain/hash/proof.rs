use super::sha3::{Sha3_512, Digest};

pub fn proof_of_work(last_proof: u32) -> u32 {
    let mut proof = 0;
    loop {
        if valid_proof(last_proof, proof) {
            break;
        }

        proof += 1;
    }

    proof
}

pub fn valid_proof(last_proof: u32, proof: u32) -> bool {
    let mut hasher = Sha3_512::new();
    hasher.input(&proofs_to_bytes(last_proof, proof));
    let hash = hasher.result();

    match hash.as_slice()[hash.len()-2..] {
        [0, 0] => true,
        _ => false,
    }
}

fn proofs_to_bytes(last_proof: u32, proof: u32) -> [u8;8] {
    let mut proofs: u64 = ((last_proof.to_le() as u64) << 32) + (proof.to_le() as u64); //to_le converts to little endian
    let mut bytes = [0u8; 8];
    for i in (0..8).rev() {
        bytes[i] = (proofs & 0xff) as u8;
        proofs >>= 8;
    }

    bytes
}

extern crate test;

#[test]
fn test_proofs_to_bytes() {
    assert_eq!(proofs_to_bytes(42, 12), [0, 0, 0, 42, 0, 0, 0, 12]);
}

#[bench]
fn bench_valid_proof(b: &mut test::Bencher) {
    b.iter(|| valid_proof(25565, 12345));
}