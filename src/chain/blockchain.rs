extern crate sha3;
extern crate bincode;

use self::sha3::{Sha3_512, Digest};

use std::time::{SystemTime, UNIX_EPOCH};

use super::block::Block;
use super::transaction::Transaction;
use super::hash::proof::{proof_of_work,valid_proof};
use super::chainsize::ChainSize;

pub struct Blockchain {
    chain: Vec<Block>,
    current_transactions: Vec<Transaction>,
}

impl Blockchain {
    pub fn new() -> Blockchain {
        let mut chain = Blockchain {
            chain: Vec::new(),
            current_transactions: Vec::new(),
        };
        let proof = proof_of_work(0);
        chain.new_block(
            proof,
            vec![0; 128]
        );

        chain
    }

    pub fn new_transaction(&mut self, transaction: Transaction) {
        self.current_transactions.push(transaction);
    }

    pub fn head(&self) -> Block {
        self.chain[self.chain.len() - 1].clone()
    }

    pub fn get_chain(&self) -> Vec<Block> {
        self.chain.clone()
    }

    pub fn get_chain_len(&self) -> ChainSize {
        ChainSize::new(self.chain.len())
    }

    pub fn create_block(&mut self) {
        let proof;
        let previous_hash;
        {
            let prev = &self.chain[self.chain.len() - 1];

            proof = proof_of_work(prev.get_proof());

            let prev_bytes = bincode::serialize(prev).unwrap();
            let mut hasher = Sha3_512::new();
            hasher.input(&prev_bytes);
            previous_hash = hasher.result().to_vec();
        }
        self.new_block(proof, previous_hash);
    }

    fn new_block(&mut self, proof: u32, previous_hash: Vec<u8>) {
        let idx = (self.chain.len() + 1) as u32;

        let start = SystemTime::now();
        let epoch_duration = start.duration_since(UNIX_EPOCH).expect("Negative time delta");
        let epoch_ms = epoch_duration.as_secs() * 1000 +
            epoch_duration.subsec_nanos() as u64 / 1_000_000;

        let block = Block::create(idx, epoch_ms, self.current_transactions.clone(), proof, previous_hash);

        self.current_transactions = Vec::new();

        self.chain.push(block);
    }

    pub fn accept_new_block(&mut self, block: Block) -> bool {
        let head = self.head();
        if valid_proof(head.get_proof(), block.get_proof()){
            self.chain.push(block);
            true
        }
        else {
            false
        }
    }
}