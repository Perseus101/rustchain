#![feature(plugin, test)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;

#[macro_use] extern crate serde_derive;

use rocket::State;
use rocket_contrib::Json;

mod chain;

use chain::transaction::Transaction;
use chain::block::Block;
use chain::chainsize::ChainSize;

mod manager;
use manager::chainmanager::ChainManager;
use manager::peer::Peer;

#[get("/head")]
fn head(chain: State<ChainManager>) -> Json<Block> {
    Json(chain.inner().head())
}

#[get("/chain")]
fn chain(chain: State<ChainManager>) -> Json<Vec<Block>> {
    Json(chain.inner().get_chain())
}

#[get("/block/<index>")]
fn block(index: usize, chain: State<ChainManager>) -> Json<Block> {
    Json(chain.inner().get_block(index))
}

#[get("/len")]
fn chain_len(chain: State<ChainManager>) -> Json<ChainSize> {
    Json(chain.inner().get_chain_len())
}

#[get("/mine")]
fn mine(chain: State<ChainManager>) -> Json<Block> {
    chain.inner().create_block();

    Json(chain.inner().head())
}

#[post("/transaction", data = "<transaction>")]
fn new_transaction(transaction: Json<Transaction>, chain: State<ChainManager>) {
    chain.inner().new_transaction(transaction.into_inner());
}

#[get("/peer/resolve")]
fn resolve(chain: State<ChainManager>) -> Json<ChainSize> {
    Json(chain.inner().resolve())
}

#[post("/peer/register", data = "<peer>")]
fn new_peer(peer: Json<Peer>, chain: State<ChainManager>) {
    chain.inner().add_peer(peer.into_inner());
}

fn main() {
    rocket::ignite()
        .mount("/", routes![head, chain, block, chain_len, mine, new_transaction, resolve, new_peer])
        .manage(ChainManager::new())
        .launch();
}