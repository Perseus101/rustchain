use std::sync::RwLock;

use chain::blockchain::Blockchain;
use chain::chainsize::ChainSize;
use chain::transaction::Transaction;
use chain::block::Block;

use manager::peermanager::PeerManager;
use manager::peer::Peer;

pub struct ChainManager {
    chain: RwLock<Blockchain>,
    peers: RwLock<PeerManager>,
}

impl ChainManager {
    pub fn new() -> ChainManager {
        ChainManager {
            chain: RwLock::from(Blockchain::new()),
            peers: RwLock::from(PeerManager::new()),
        }
    }

    // Passthrough functions to Blockchain
    pub fn head(&self) -> Block {
        self.chain.read().unwrap().head()
    }

    pub fn get_chain(&self) -> Vec<Block> {
        self.chain.read().unwrap().get_chain()
    }

    pub fn get_block(&self, index: usize) -> Block {
        self.chain.read().unwrap().get_chain()[index].clone()
    }

    pub fn get_chain_len(&self) -> ChainSize {
        self.chain.read().unwrap().get_chain_len()
    }

    pub fn new_transaction(&self, transaction: Transaction) {
        self.chain.write().unwrap().new_transaction(transaction);
    }

    pub fn create_block(&self) {
        self.chain.write().unwrap().create_block()
    }

    // Peer functions
    pub fn add_peer(&self, peer: Peer) {
        self.peers.write().unwrap().add_peer(peer);
    }

    pub fn resolve(&self) -> ChainSize {
        let peers = &self.peers.read().unwrap().peers;
        let peer_lengths: Vec<(&Peer, usize)> = peers.iter().map(|peer| (peer, peer.get_chain_len().get_size())).collect();

        let mut longest_peer = None;
        let chain_length: usize;
        let mut longest_length;
        {
            chain_length = self.chain.read().unwrap().get_chain_len().get_size();
            longest_length = chain_length;
            for (peer, length) in peer_lengths.iter() {
                if *length > longest_length {
                    longest_peer = Some(peer);
                    longest_length = *length;
                }
            }
        }
        if let Some(peer) = longest_peer {
            let mut chain = self.chain.write().unwrap();
            for i in chain_length..longest_length {
                chain.accept_new_block(peer.get_block(i));
            }
        }
        self.chain.read().unwrap().get_chain_len()
    }
}