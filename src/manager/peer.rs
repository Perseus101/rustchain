extern crate restson;
use self::restson::{RestClient,RestPath,Error};

use chain::chainsize::ChainSize;
use chain::block::Block;

impl RestPath<()> for ChainSize {
    fn get_path(_: ()) -> Result<String,Error> { Ok(String::from("len")) }
}

impl RestPath<usize> for Block {
    fn get_path(param: usize) -> Result<String,Error> { Ok(format!("block/{}", param)) }
}

#[derive(Deserialize)]
pub struct Peer {
    client_url: String
}

impl Peer {
    pub fn get_chain_len(&self) -> ChainSize {
        let mut client = RestClient::new(&self.client_url).unwrap();
        client.get(()).unwrap()
    }

    pub fn get_block(&self, index: usize) -> Block {
        let mut client = RestClient::new(&self.client_url).unwrap();
        client.get(index).unwrap()
    }
}