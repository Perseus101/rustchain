FROM rustlang/rust:nightly
EXPOSE 4200

WORKDIR /usr/src/myapp
COPY . .

RUN cargo install

CMD ["rustchain"]